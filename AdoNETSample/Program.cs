﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Extensions.Configuration;


namespace AdoNETSample
{
    public static class Program
    {
        public static void Main()
        {
            Console.WriteLine("Hello ADO.NET");
            var builder = new ConfigurationBuilder();

            builder.SetBasePath(Directory.GetCurrentDirectory());

            builder.AddJsonFile("appsettings.json");

            var config = builder.Build();

            string connectionString = config.GetConnectionString("DefaultConnection");
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                Console.WriteLine("Connection opened");
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                DBController controller = new DBController();
                string button = string.Empty;
                while (button != "q")
                {
                    Console.WriteLine("Enter q to exit, a to adding records, d to deleting records, u to update records, g to getting all recods.");
                    button = Console.ReadLine();
                    string result = string.Empty;
                    switch (button)
                    {
                        case "a":
                            {
                                Console.WriteLine("Enter Name to add in database:");
                                string recordName = Console.ReadLine();
                                Console.WriteLine("Enter Adress to add in database:");
                                string recordAdress = Console.ReadLine();
                                result = controller.AddingRecord(ref connection, recordName, recordAdress);
                                break;
                            }
                        case "g":
                            {
                                controller.GetAllRecords(ref connection);
                                break;
                            }
                        case "d":
                            {
                                Console.WriteLine("Enter Name to delete in database:");
                                string recordName = Console.ReadLine();
                                result = controller.DeleteRecord(ref connection, recordName);
                                break;
                            }
                        case "u":
                            {
                                Console.WriteLine("Enter Name to update in database:");
                                string recordName = Console.ReadLine();
                                Console.WriteLine("Enter Adress to update in database:");
                                string recordAdress = Console.ReadLine();
                                result = controller.UpdateRecord(ref connection, recordName, recordAdress);
                                break;
                            }
                    }
                    Console.WriteLine(result);
                }
                Console.WriteLine("Connection close");
                connection.Close();
            }
        }
    }
}
