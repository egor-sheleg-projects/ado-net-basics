﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace AdoNETSample
{
    /// <summary>
    /// All database user functions.
    /// </summary>
    public class DBController
    {
        /// <summary>
        /// Add recod to database.
        /// </summary>
        /// <param name="connection">Referance sql connection.</param>
        public string AddingRecord(ref SqlConnection connection, string recordName, string recordAdress)
        {
            SqlCommand sqlCommand = new SqlCommand("INSERT INTO Customers (Name, Adress) VALUES (@record1, @record2)", connection);
            sqlCommand.Parameters.AddWithValue("@record1", recordName);
            sqlCommand.Parameters.AddWithValue("@record2", recordAdress);
            string result = "Not done.";
            try
            {
                sqlCommand.ExecuteNonQuery();
                result = "Done.";
            }
            catch (Exception)
            {
                throw new ArgumentException("Wrong Name or Adress");
            }

            return result;
        }

        /// <summary>
        /// Get all recods from database.
        /// </summary>
        /// <param name="connection">Referance sql connection.</param>
        public void GetAllRecords(ref SqlConnection connection)
        {
            List<Dictionary<string, string>> rows = new List<Dictionary<string, string>>();
            Dictionary<string, string> column;
            string sqlQuery = "SELECT Name, Adress FROM Customers";

            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    column = new Dictionary<string, string>();

                    column["Name"] = reader["Name"].ToString();
                    column["Adress"] = reader["Adress"].ToString();

                    rows.Add(column);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (Dictionary<string, string> columnn in rows)
            {
                Console.Write(columnn["Name"] + " ");
                Console.Write(columnn["Adress"] + " ");
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Delete recod from database.
        /// </summary>
        /// <param name="connection">Referance sql connection.</param>
        public string DeleteRecord(ref SqlConnection connection, string recordName)
        {
            SqlCommand sqlCommand = new SqlCommand("DELETE FROM Customers WHERE CONVERT(VARCHAR, Name)=@record1", connection);
            sqlCommand.Parameters.AddWithValue("@record1", recordName);
            string result = "Not done.";
            try
            {
                sqlCommand.ExecuteNonQuery();
                result = "Done.";
            }
            catch (Exception)
            {
                throw new ArgumentException("Wrong Name or Adress");
            }

            return result;
        }

        /// <summary>
        /// Update recod in database.
        /// </summary>
        /// <param name="connection">Referance sql connection.</param>
        public string UpdateRecord(ref SqlConnection connection, string recordName, string recordAdress)
        {
            SqlCommand sqlCommand = new SqlCommand("UPDATE Customers SET Adress=@record2 WHERE CONVERT(VARCHAR, Name)=@record1", connection);
            sqlCommand.Parameters.AddWithValue("@record1", recordName);
            sqlCommand.Parameters.AddWithValue("@record2", recordAdress);
            string result = "Not done.";
            try
            {
                sqlCommand.ExecuteNonQuery();
                result = "Done.";
            }
            catch (Exception)
            {
                throw new ArgumentException("Wrong Name or Adress");
            }

            return result;
        }
    }
}
