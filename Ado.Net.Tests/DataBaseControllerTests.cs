﻿using System;
using System.Data.SqlClient;
using System.IO;
using AdoNETSample;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ado.Net.Tests
{
    /// <summary>
    /// Class to test all DataBaseController features.
    /// </summary>
    [TestClass]
    public class DataBaseControllerTests
    {
        /// <summary>
        /// Testing deleting from database.
        /// </summary>
        [TestMethod]
        public void DeleteFromDBTest()
        {
            var builder = new ConfigurationBuilder();

            builder.SetBasePath(Directory.GetCurrentDirectory());

            builder.AddJsonFile("appsettings.json");

            var config = builder.Build();

            string connectionString = config.GetConnectionString("DefaultConnection");
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                Console.WriteLine("Connection opened");
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // arrange 
                var dbcontroller = new DBController();
                string arg1 = "Name";
                string expected = "Done.";

                // act
                string result = dbcontroller.DeleteRecord(ref connection, arg1);

                // assert            
                Assert.AreEqual(expected, result);
            }
        }

        /// <summary>
        /// Testing adding to database.
        /// </summary>
        [TestMethod]
        public void AddingToDBTest()
        {
            var builder = new ConfigurationBuilder();

            builder.SetBasePath(Directory.GetCurrentDirectory());

            builder.AddJsonFile("appsettings.json");

            var config = builder.Build();

            string connectionString = config.GetConnectionString("DefaultConnection");
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                Console.WriteLine("Connection opened");
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // arrange 
                var dbcontroller = new DBController();
                string arg1 = "Name";
                string arg2 = "Adress";
                string expected = "Done.";

                // act
                string result = dbcontroller.AddingRecord(ref connection, arg1, arg2);

                // assert            
                Assert.AreEqual(expected, result);
            }
        }

        /// <summary>
        /// Testing update in database.
        /// </summary>
        [TestMethod]
        public void UpdateInDBTest()
        {
            var builder = new ConfigurationBuilder();

            builder.SetBasePath(Directory.GetCurrentDirectory());

            builder.AddJsonFile("appsettings.json");

            var config = builder.Build();

            string connectionString = config.GetConnectionString("DefaultConnection");
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                Console.WriteLine("Connection opened");
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // arrange 
                var dbcontroller = new DBController();
                string arg1 = "Name";
                string arg2 = "New Adress";
                string expected = "Done.";

                // act
                string result = dbcontroller.UpdateRecord(ref connection, arg1, arg2);

                // assert            
                Assert.AreEqual(expected, result);
            }
        }
    }
}
